
import express, { Request, Response } from 'express'

const server = express()

server.get('/', (_req: Request, res: Response) => {
    res.send('OK:2')
})

const PORT = process.env.PORT || 3000
server.listen(PORT, () => {
    console.log('Server listening port', PORT)
})
